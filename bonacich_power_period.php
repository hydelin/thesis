<?php
require_once 'config.php';
require_once 'menu.php';

$query = "SELECT min(year) as minYear, max(year) as maxYear FROM codes WHERE year != '0000'";
$result = mysql_query($query);
$row = mysql_fetch_array($result, MYSQL_ASSOC);
$minYear = $row['minYear'];
$maxYear = $row['maxYear'];
mysql_free_result($result);




$iterative = isset($_GET['iterative']) ? $_GET['iterative'] : 10; // 遞迴
$beta = isset($_GET['beta']) ? $_GET['beta'] : 5; // 遞迴


?>
<br />
<center>
<form method="GET" id="f">

    遞回
        <select name="iterative" style="width:50px" onChange="document.getElementById('f').submit();">
            <?php 
                for($i=0;$i<=10;$i++)
                {
                    if($i == $iterative)
                        echo '<option selected="selected" value="'.$i.'">'.$i.'</option>';
                    else
                        echo '<option value="'.$i.'">'.$i.'</option>';
                }
            ?>
        </select>
    次
    ，beta
        <select name="beta" style="width:50px" onChange="document.getElementById('f').submit();">
            <?php 
                for($i=10;$i>=-10;$i-=1)
                {
                    if($i == $beta)
                        echo '<option selected="selected" value="'.$i.'">'.($i/10).'</option>';
                    else
                        echo '<option value="'.$i.'">'.($i/10).'</option>';
                }
            ?>
        </select>
    
</form>
</center>
<?php
foreach($GLOBAL_PERIODS as $key => $period)
{
    $power[$key] = bonacich_power($period['start'], $period['end'],  intval($iterative), intval($beta)/10, 1);
    arsort($power[$key]);
    $z[$key] = normalize($power[$key]);
}

echo '<table width="'.(count($power)*350).'" border=0 cellspacing="1" cellpadding="5" style="border-left:1px solid #CCCCCC;border-top:1px solid #CCCCCC;" align="center">';
echo '<tr>';
foreach($GLOBAL_PERIODS as $period)
{
    echo '<td colspan="3" align="center" style="color:white;background:#333333;"><b>'.$period['start'].'~'.$period['end'].'</b></td>';
}
echo '</tr>';
echo '<tr>';
foreach($GLOBAL_PERIODS as $period)
{
    echo '<td width="14%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
          <td width="3%" style="background:#EEEEEE;color:#333333;font-weight:bold;">power</td>
          <td width="3%" style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td>';
}
echo '</tr>';

for($j=0 ; $j<count($power[count($power)-1]) ; $j++)
{
    echo '<tr>';
    for($i=0 ; $i<count($power) ; $i++)
    {
        echo '<td style="color:'.(current($z[$i]) >= $GLOBAL_Z ? '000000' : '#999999').';">'.key($power[$i]).'&nbsp;</td>
              <td style="color:'.(current($z[$i]) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($power[$i]) ,3,'.','').'</td>
              <td style="color:'.(current($z[$i]) >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format(current($z[$i]) ,3,'.','').'</td>';
        next($power[$i]);
        next($z[$i]);              
    }
    echo '</tr>';
}
echo '</table>';




mysql_close($link);

?>

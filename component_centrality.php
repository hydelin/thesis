<?php
require_once 'config.php';
require_once 'menu.php';

$query = "SELECT min(year) as minYear, max(year) as maxYear FROM codes WHERE year != '0000'";
$result = mysql_query($query);
$row = mysql_fetch_array($result, MYSQL_ASSOC);
$minYear = $row['minYear'];
$maxYear = $row['maxYear'];
mysql_free_result($result);


$firstYear = $minYear;
if(isset($_GET['periods']))
{
    $periods = $_GET['periods'];
}
else
{
    $periods = array();
    for($i=$minYear ; $i<=$maxYear ; $i++)
    {
        array_push($periods,$i);
    }
}


if(!isset($_GET['metric'])) $_GET['metric'] = array();


$data = array();
foreach($component as $key => $value)
{
    $data['['.$value.']'.$key] = array();
    
    foreach($periods as  $year)
    {
        $data['['.$value.']'.$key][$year] = '&nbsp;';
    }
}

function aaaa(& $data, $isAccumulated=true, $unit=3, $color='black')
{
    global $GLOBAL_Z;
    global $periods;
    global $firstYear;
    
    foreach($periods as $key => $year)
    {    
        if($isAccumulated)
        {
            $currentStartYear = $firstYear;
        }
        else
        {
            if($key == 0)
            {
                $currentStartYear = $firstYear;
            }
            else
            {
                $currentStartYear = $periods[$key-1] + 1;
            }
        }
            
    
        $currentEndYear = $year;
        
        $positive_power = bonacich_power($currentStartYear, $currentEndYear, 5, 0.5, 1, $unit, 1);
        $negative_power = bonacich_power($currentStartYear, $currentEndYear, 5, -0.5, 1, $unit, 1);
        $betweenness = betweenness($currentStartYear,$currentEndYear, $unit, 1);
        $degree = degree($currentStartYear, $currentEndYear, $unit, 1);
        $indegree = $degree['in'];
        $outdegree = $degree['out'];
        $alldegree = $degree['all'];
        $closeness = closeness($currentStartYear, $currentEndYear, $unit, 1);
        $inCloseness = $closeness['inCloseness'];
        $outCloseness = $closeness['outCloseness'];
    
        arsort($positive_power);
        arsort($negative_power);
        arsort($betweenness);
        arsort($indegree);
        arsort($outdegree);
        arsort($alldegree);
        arsort($inCloseness);
        arsort($outCloseness);
    
        $pz = normalize($positive_power);
        $nz = normalize($negative_power);
        $bz = normalize($betweenness);
        $iz = normalize($indegree);
        $oz = normalize($outdegree);
        $az = normalize($alldegree);
        $icz = normalize($inCloseness);
        $ocz = normalize($outCloseness);
    
    
        for($j=0 ; $j<count($alldegree) ; $j++)
        {
            if(current($pz) > $GLOBAL_Z && in_array(1, $_GET['metric']))
            {
              $data[key($positive_power)][$year] .= '<font color='.$color.'>P</font>&nbsp;';
            }   
            if(current($nz) > $GLOBAL_Z && in_array(2, $_GET['metric']))
            {
                $data[key($negative_power)][$year] .= '<font color='.$color.'>N</font>&nbsp;';
            }   
            if(current($bz) > $GLOBAL_Z && in_array(3, $_GET['metric']))
            {
                $data[key($betweenness)][$year] .= '<font color='.$color.'>B</font>&nbsp;';
            }   
            if(current($icz) > $GLOBAL_Z && in_array(4, $_GET['metric']))
            {
                $data[key($inCloseness)][$year] .= '<font color='.$color.'>I</font>&nbsp;';
            }   
            if(current($ocz) > $GLOBAL_Z && in_array(5, $_GET['metric']))
            {
                $data[key($outCloseness)][$year] .= '<font color='.$color.'>O</font>&nbsp;';
            }   
            if(current($iz) > $GLOBAL_Z && in_array(6, $_GET['metric']))
            {
                $data[key($indegree)][$year] .= '<font color='.$color.'>i</font>&nbsp;';
            }   
            if(current($oz) > $GLOBAL_Z && in_array(7, $_GET['metric']))
            {
                $data[key($outdegree)][$year] .= '<font color='.$color.'>o</font>&nbsp;';
            }   
            if(current($az) > $GLOBAL_Z && in_array(8, $_GET['metric']))
            {
                $data[key($alldegree)][$year] .= '<font color='.$color.'>A</font>&nbsp;';
            }   
    
            next($positive_power);
            next($negative_power);
            next($betweenness);
            next($indegree);
            next($outdegree);
            next($alldegree);
            next($inCloseness);
            next($outCloseness);
            
            next($pz);
            next($nz);
            next($bz);
            next($iz);
            next($oz);
            next($az);
            next($icz);
            next($ocz);
            
        }
    }
}

?>
<center>
<form method='GET' style="font-size:13px;">
    <input type="checkbox" name="type1" value="1" <?php echo isset($_GET['type1']) ? 'checked="checked"' : ''; ?>/> <?php echo isset($_GET['type1']) ? 'CDF - 以事件為單位' : '<font color="gray">CDF - 以事件為單位</font>'; ?>
    <input type="checkbox" name="type2" value="1" <?php echo isset($_GET['type2']) ? 'checked="checked"' : ''; ?>/> <?php echo isset($_GET['type2']) ? 'CDF - 以年為單位' : '<font color="gray">CDF - 以年為單位</font>'; ?>
    <input type="checkbox" name="type3" value="1" <?php echo isset($_GET['type3']) ? 'checked="checked"' : ''; ?>/> <?php echo isset($_GET['type3']) ? 'CDF - 以因果為單位' : '<font color="gray">CDF - 以因果為單位</font>'; ?>
    <input type="checkbox" name="type4" value="1" <?php echo isset($_GET['type4']) ? 'checked="checked"' : ''; ?>/> <?php echo isset($_GET['type4']) ? 'PDF - 以事件為單位' : '<font color="gray">PDF - 以事件為單位</font>'; ?>
    <input type="checkbox" name="type5" value="1" <?php echo isset($_GET['type5']) ? 'checked="checked"' : ''; ?>/> <?php echo isset($_GET['type5']) ? 'PDF - 以年為單位' : '<font color="gray">PDF - 以年為單位</font>'; ?>
    <input type="checkbox" name="type6" value="1" <?php echo isset($_GET['type6']) ? 'checked="checked"' : ''; ?>/> <?php echo isset($_GET['type6']) ? 'PDF - 以因果為單位' : '<font color="gray">PDF - 以因果為單位</font>'; ?>
<br />
    <input type="checkbox" name="metric[]" value="1" <?php echo in_array(1,$_GET['metric']) ? 'checked="checked"' : ''; ?>/> <?php echo in_array(1,$_GET['metric']) ? 'power+' : '<font color="gray">power+</font>'; ?>
    <input type="checkbox" name="metric[]" value="2" <?php echo in_array(2,$_GET['metric']) ? 'checked="checked"' : ''; ?>/> <?php echo in_array(2,$_GET['metric']) ? 'power-' : '<font color="gray">power-</font>'; ?>
    <input type="checkbox" name="metric[]" value="3" <?php echo in_array(3,$_GET['metric']) ? 'checked="checked"' : ''; ?>/> <?php echo in_array(3,$_GET['metric'])? 'betw' : '<font color="gray">betw</font>'; ?>
    <input type="checkbox" name="metric[]" value="4" <?php echo in_array(4,$_GET['metric']) ? 'checked="checked"' : ''; ?>/> <?php echo in_array(4,$_GET['metric']) ? 'inCloseness' : '<font color="gray">inCloseness</font>'; ?>
    <input type="checkbox" name="metric[]" value="5" <?php echo in_array(5,$_GET['metric']) ? 'checked="checked"' : ''; ?>/> <?php echo in_array(5,$_GET['metric']) ? 'outCloseness' : '<font color="gray">outCloseness</font>'; ?>
    <input type="checkbox" name="metric[]" value="6" <?php echo in_array(6,$_GET['metric']) ? 'checked="checked"' : ''; ?>/> <?php echo in_array(6,$_GET['metric']) ? 'inDegree' : '<font color="gray">inDegree</font>'; ?>
    <input type="checkbox" name="metric[]" value="7" <?php echo in_array(7,$_GET['metric']) ? 'checked="checked"' : ''; ?>/> <?php echo in_array(7,$_GET['metric']) ? 'outDegree' : '<font color="gray">outDegree</font>'; ?>
    <input type="checkbox" name="metric[]" value="8" <?php echo in_array(8,$_GET['metric']) ? 'checked="checked"' : ''; ?>/> <?php echo in_array(8,$_GET['metric']) ? 'allDegree' : '<font color="gray">allDegree</font>'; ?>
<br />
<?php

for($i=$minYear ; $i<=$maxYear ; $i++)
{
    if(in_array($i,$periods))
    {
        echo '<input type="checkbox" name="periods[]" value="'.$i.'" checked="checked" />'.$i;
    }
    else
    {
        echo '<input type="checkbox" name="periods[]" value="'.$i.'"/><font color="gray">'.$i.'</font>';
    }
}

?>   
<br />
<br /> 
    <input type="submit"/>
<form>
<br />
<br />
</center>
<?php

if(isset($_GET['type1'])) aaaa($data, true, 1, 'red'); // CDF - 以事件為單位
if(isset($_GET['type2'])) aaaa($data, true, 2, 'red'); // CDF - 以年為單位
if(isset($_GET['type3'])) aaaa($data, true, 3, 'red'); // CDF - 以因果為單位
if(isset($_GET['type4'])) aaaa($data, false, 1, 'blue'); // PDF - 以事件為單位
if(isset($_GET['type5'])) aaaa($data, false, 2, 'blue'); // PDF - 以年為單位
if(isset($_GET['type6'])) aaaa($data, false, 3, 'blue'); // PDF - 以因果為單位

echo '<table border=0 cellspacing="1" cellpadding="5" style="border-left:1px solid #CCCCCC;border-top:1px solid #CCCCCC;font-size:13px;" align="center">';
echo '<caption valign="bottom" align="left"><font color="red">■</font> CDF&nbsp;&nbsp;<font color="blue">■</font> PDF</caption>';
echo '<tr style="background:#333333;color:white;"><td>&nbsp;</td>';
foreach($periods as $key => $year)
{
    if($key == 0)
    {
        $startYear = $firstYear;
        $endYear = $year;
    }
    else
    {
        $startYear = $periods[$key-1] + 1;
        $endYear = $year;
    }
    
    if($startYear == $endYear)
    {
        echo '<td width="60">'.$endYear.'</td>';
    }
    else
    {
        echo '<td width="60">'.$startYear.'~'.$endYear.'</td>';
    }
        
}
echo '</td>';
foreach($data as $component => $data2)
{
    echo '<tr>';
    echo '<td>'.$component.'</td>';
    foreach($data2 as $year => $matrix)
    {
         echo '<td style="font-weight:bold">'.$matrix.'</td>';
    }
    echo '</tr>';
}
echo '</table>';

mysql_close($link);

?>
<?php
require_once 'config.php';
require_once 'menu.php';

$query = "SELECT min(year) as minYear, max(year) as maxYear FROM codes WHERE year != '0000'";
$result = mysql_query($query);
$row = mysql_fetch_array($result, MYSQL_ASSOC);
$minYear = $row['minYear'];
$maxYear = $row['maxYear'];
mysql_free_result($result);



/*
 *  Parameter Setting
 */ 
 
$startYear = isset($_GET['startYear']) ? $_GET['startYear'] : $minYear; // 統計起始年
$endYear = isset($_GET['endYear']) ? $_GET['endYear'] : $maxYear; // 統計結束年
$interval = isset($_GET['interval']) ? $_GET['interval'] : 1; // 每幾年唯一個時期
$isAccumulated = isset($_GET['isAccumulated']) && $_GET['isAccumulated'] == 'N' ? false : true; // 是否累計
$iterative = isset($_GET['iterative']) ? $_GET['iterative'] : 2; // 遞迴
$beta = isset($_GET['beta']) ? $_GET['beta'] : 5; // 遞迴


function display($startYear, $endYear)
{
    global $iterative;
    global $beta;
    global $GLOBAL_Z;    
    
    $power = bonacich_power($startYear, $endYear, intval($iterative), intval($beta)/10, 1, 3 , 1);
    //$power = bonacich_power($query, intval($iterative), -0.1, 1);
    
    arsort($power);
    
    $z = normalize($power);
    

    echo '<table width="600" border=0 cellspacing="1" cellpadding="5" style="border-left:1px solid #CCCCCC;border-top:1px solid #CCCCCC;" align="center">';
    echo '<tr><td colspan="3" align="center" style="color:white;background:#333333;"><b>'.$startYear.' ~ '.$endYear.'</b></td></tr>';
    echo '<tr><td width="70%" style="background:#EEEEEE;color:#333333;font-weight:bold;">主軸</td>
              <td width="15%" style="background:#EEEEEE;color:#333333;font-weight:bold;">power</td>
              <td style="background:#EEEEEE;color:#333333;font-weight:bold;">Z值</td></tr>';
    foreach($power as $code => $p)
    {
    
//        if($p == 0) break;
        echo '<tr>';
        echo '<td style="color:'.($z[$code] >= $GLOBAL_Z ? '000000' : '#999999').';">'.$code.'</td>
              <td style="color:'.($z[$code] >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format($p ,3,'.','').'</td>
              <td style="color:'.($z[$code] >= $GLOBAL_Z ? 'red' : '#999999').';">'.number_format($z[$code] ,3,'.','').'</td>';
        echo '</tr>';
    }
    echo '</table>';
}



?>
<br />
<center>
<form method="GET" id="f">
    時間:
        <select name="startYear" style="width:70px" onChange="document.getElementById('f').submit();">
            <?php 
                for($year=$minYear;$year<=$maxYear;$year++)
                {
                    if($year == $startYear)
                        echo '<option selected="selected" value="'.$year.'">'.$year.'</option>';
                    else
                        echo '<option value="'.$year.'">'.$year.'</option>';
                }
            ?>
        </select>
    年~
        <select name="endYear" style="width:70px" onChange="document.getElementById('f').submit();">
            <?php 
                for($year=$minYear;$year<=$maxYear;$year++)
                {
                    if($year == $endYear)
                        echo '<option selected="selected" value="'.$year.'">'.$year.'</option>';
                    else
                        echo '<option value="'.$year.'">'.$year.'</option>';
                }
            ?>
        </select>
    年，        
    間隔:
        <select name="interval" style="width:50px" onChange="document.getElementById('f').submit();">
            <?php 
                for($i=1;$i<=($maxYear-$minYear+2)/2;$i++)
                {
                    if($i == $interval)
                        echo '<option selected="selected" value="'.$i.'">'.$i.'</option>';
                    else
                        echo '<option value="'.$i.'">'.$i.'</option>';
                }
            ?>
        </select>
    年
    ，是否累計:
        <select name="isAccumulated" style="width:50px" onChange="document.getElementById('f').submit();">
            <option value="Y" <?php echo $isAccumulated ? 'selected="selected"':''; ?>>是</option>
            <option value="N" <?php echo !$isAccumulated ? 'selected="selected"':''; ?>>否</option>
        </select>
    ，遞回
        <select name="iterative" style="width:50px" onChange="document.getElementById('f').submit();">
            <?php 
                for($i=0;$i<=10;$i++)
                {
                    if($i == $iterative)
                        echo '<option selected="selected" value="'.$i.'">'.$i.'</option>';
                    else
                        echo '<option value="'.$i.'">'.$i.'</option>';
                }
            ?>
        </select>
    次
    ，beta
        <select name="beta" style="width:50px" onChange="document.getElementById('f').submit();">
            <?php 
                for($i=10;$i>=-10;$i-=1)
                {
                    if($i == $beta)
                        echo '<option selected="selected" value="'.$i.'">'.($i/10).'</option>';
                    else
                        echo '<option value="'.$i.'">'.($i/10).'</option>';
                }
            ?>
        </select>
    
</form>
</center>
<?php

for($year=$startYear ; $year<=$endYear ; $year+=$interval)
{
    if($isAccumulated)
    {
        $currentStartYear = $startYear;
    }
    else
    {
        $currentStartYear = $year;
    }
    
    $currentEndYear = ($year + $interval-1 > $endYear ? $endYear : $year + $interval-1 );
    
    display( $currentStartYear, $currentEndYear);    
}


mysql_close($link);

?>
